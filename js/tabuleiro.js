function tabuleiro_inicial() {
    return [{
            id: 0,
            nome: "Casa 1",
            pergunta: "?",
            posicao_x: "200px",
            posicao_y: "225px"
        },
        {
            id: 1,
            nome: "Casa 2",
            pergunta: "?",
            posicao_x: "140px",
            posicao_y: "320px"
        },
        {
            id: 2,
            nome: "Casa 3",
            pergunta: "?",
            posicao_x: "120px",
            posicao_y: "410px"
        },
        {
            id: 3,
            nome: "Casa 3",
            pergunta: "?",
            posicao_x: "125px",
            posicao_y: "510px"
        },
        {
            id: 4,
            nome: "Casa 4",
            pergunta: "?",
            posicao_x: "190px",
            posicao_y: "580px"
        },
        {
            id: 5,
            nome: "Casa 5",
            pergunta: "?",
            posicao_x: "300px",
            posicao_y: "590px"
        },
        {
            id: 6,
            nome: "Casa 6",
            pergunta: "?",
            posicao_x: "380px",
            posicao_y: "530px"
        },
    ]
};
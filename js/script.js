const demo = {
    //VARIAVEIS
    tabuleiro: [],
    canvas: null,
    id_casa_atual: null,
    tempo: null,
    tempo_resposta_padrao: null,

    //FUNCOES
    iniciar(canvas) {
        this.canvas = canvas;
        this.tabuleiro = tabuleiro_inicial();
        this.id_casa_atual = 0;
        this.desenhar();
    },

    desenhar_casa(casa) {
        let classe = "casa";
        if (this.id_casa_atual == casa.id) {
            classe = "bita";
        }
        let div = "<div class='" + classe + "' style='top:" + casa.posicao_y + "; left:" + casa.posicao_x + "' onclick='demo.avancar_casa(" + casa.id + ")'></div>";

        return div;
    },

    desenhar() {
        this.canvas.innerHTML =
            this.tabuleiro
            .map((element) => this.desenhar_casa(element))
            .reduce((content, current) => content + current);
    },

    avancar_casa(id) {
        // if ((id != this.id_casa_atual + 1) ||
        //     document.querySelector(".pergunta") !== null) {
        //     return;
        // }
        this.id_casa_atual = id;
        this.desenhar();
        this.mostrar_pergunta(id);
    },

    mostrar_pergunta(id) {
        this.canvas.innerHTML +=
            "<div class='pergunta' onclick='demo.ocultar_pergunta()'></div>";
        var elem = document.querySelector(".pergunta");
        var height = 0;
        var width = 0;
        var interval = setInterval(aumentar_div_pergunta, 10);

        function aumentar_div_pergunta() {
            if (height == 350) {
                clearInterval(interval);
            } else {
                height += 10;
                width += 20;
                elem.style.height = height + 'px';
                elem.style.width = width + 'px';
            }
        }
    },

    ocultar_pergunta() {
        document.querySelector(".pergunta").remove();
    }
};